"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
inference.py:
    Gathers the image data from the application container using gRPC service "MainServer" for channel "getStream.
    Reply back the predicted result as an response to the image data request."
"""

import json
import numpy as np
import time
import cv2
import tvm
from concurrent import futures
import grpc
import multiprocessing

# ARMCSP stack package imports.
from src.argconfig import ArgConf
from src.grpc import Datas_pb2
from src.grpc import Datas_pb2_grpc
from src.load_model_config import load_model_config, load_model
from src.utils import preprocessing_img
from src.custom_exception import ModelLoadError, ModelNotFoundError
from src.logger import get_logger

def validate_args(args):
    """
    Validates the argument passed from command-line & raise the exception for non valid argument.

    Args:
        None

    Returns:
        None
    """
    if "tiny_yolov3" not in args['model'] or "yolov3" not in args['model']:
        logger.error("Model name %s is not supported.", args['model'])
        raise ModelNotFoundError("Model Not Supported.")

    if int(args['port']) == 5000 or int(args['port']) >=0 and int(args['port']) < 1024:
        logger.warning("Please use ports than 0 to 1024 & 5000.")

    if int(args['cpu_cores']) <= 0 or int(args['cpu_cores']) > multiprocessing.cpu_count():
        logger.warning("Please use proper number of cores.")
        raise ValueError("Give proper CPU core count.")


def get_args():
    """
    Load the configuration file passed as an parameter & returns the argument.

    Args:
        None

    Returns:
        args: Argument's dictionary object with required model configuration.
    """
    args = ArgConf()
    return args

# Create logger for various debugging logs.
logger = get_logger()

class Inference(Datas_pb2_grpc.MainServerServicer):                                                                         
    def __init__(self, args, debug=0):  
        """
        Inference class constructer, validates & loads the configuration for model & load the model.

        Args:
            args: Argument passed from command-prompt.
            debug: Enable debugging to print debugging logs.

        Returns:
            None
        """                                                                                                   
        # Load model configuration
        validate_args(args)
        self.debug = debug
        self.args = args
        self.model = args["model"]

        if self.model == "tiny_yolov3":
            self.config_path = "config/tiny_yolov3.json"
        elif self.model == "yolov3":
            self.config_path = "config/yolov3.json"
        else:
            logger.warning("Model not supported, loading yolov3[default] model config.")
            self.config_path = "config/yolov3.json"

        if debug:
            logger.debug("Loading model configuration %s.", self.model)
        self.config = load_model_config(self.model, args["cpu_cores"], self.config_path)
        #load model
        if debug:
            logger.debug("Model Configuration loaded.")
            logger.debug("Loading %s Model.", self.model)
        # load the model.
        start = time.time()

        self.model = load_model(self.config.model_path)
        if self.model is None:
            raise ModelLoadError("Something went wrong, unable to load model.")

        end = time.time()                                                                                                           
        self.model_load_time = (end-start)
        if debug:
            logger.debug("Model Loaded successfully, Time Taken to load: %s.", str(self.model_load_time))
        logger.info("Inferencing Module initialized successfully!")
                                                                                                                
    def getStream(self, request_iterator, context):      
        """
        Get the set of requests received in iterator & extract the camera frame, process it and send for prediction.
        Also postprocess the output results & reply it back using gRPC to the requestor.

        Args:
            request_iterator: Iterator for received requests.
            context: NA

        Yields:
            Datas_pb2.Reply(reply=msg): Yields the msg.
        """                                                                   
        for req in request_iterator:                                                                                        
            start = time.time()    
            if self.debug:
                logger.debug("Loading Frame buffer")
            dBuf = np.frombuffer(req.datas, dtype=np.uint8)
            if dBuf is None:
                logger.error("Failed while loading buffer.")
                continue                                  
            if self.debug:
                logger.debug("Decoding the jpeg buffer data to numpy array.") 
            img = cv2.imdecode(dBuf, cv2.IMREAD_COLOR)
            if img is None:
                logger.error("Failed while decoding the image.")
                continue                                      
            img = preprocessing_img(img, self.args["model"])

            self.model.set_input("input_1", tvm.nd.array(img))
            start_run = time.time()
            self.model.run()
            end_run = time.time()
            logger.info("Inference Time: %s", str(end_run - start_run))
            start_post = time.time()
            right_boxes, right_classes, right_scores = self.config.postprocess_fn(self.model, self.config.anchors, self.config.mask, self.config.strides, self.config.max_outputs, 
                                                                   self.config.iou_threshold, self.config.score_threshold, self.config.num_classes)
            end_post = time.time()
            logger.info("Postprocessing Time: %s", str(end_post - start_post))
            inference_time = 1/(end_run - start_run)                                                                                                            
            if right_boxes is None:                                                                                                                   
                msg = json.dumps({"right_boxes":0, "right_classes":0, 
                                  "right_scores":0, "model_load":str(self.model_load_time), 
                                  "inf_time":str(inference_time)})
            else:                                                                                                                                     
                msg = json.dumps({"right_boxes":right_boxes.tolist(), 
                                  "right_classes":right_classes.tolist(), 
                                  "right_scores":right_scores.tolist(), 
                                  "model_load":str(self.model_load_time), 
                                  "inf_time":str(inference_time)})
            end = time.time()            
            logger.info("Total Inference_time: %s.", str(end - start))                                                                                                                                
            yield Datas_pb2.Reply(reply=msg)                                                                                                          


def serve(args): 
    """
    Start and serve a gRPC server for Infernce worker threads.

    Args:
        args (dict): A dictionary containing model configuration arguments passed from command-line.
                     Should at least contain "port, model & cpu_cores".

    Returns:
        None
    """                                                                                                                                         
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))                                                                                  
    Datas_pb2_grpc.add_MainServerServicer_to_server(Inference(args), server) 

    server_address = '[::]:' + str(args["port"])
    server.add_insecure_port(server_address)
    server.start()
    
    logger.info('GRPC server started')
    logger.info("GRPC Port number : %s.", str(args["port"]))  

    server.wait_for_termination()                                                                                                                     
                                                                                                                                                      
if __name__ == '__main__':  
    args = get_args()                                                                                                                          
    serve(args) 


