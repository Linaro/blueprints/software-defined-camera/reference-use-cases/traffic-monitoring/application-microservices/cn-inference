class Config:
    """
    Configuration class for model settings and parameters.
    
    This class encapsulates various configuration settings and parameters used for a model.
    """
    
    def __init__(self):
        """
        Initialize configuration parameters.
        """
        self.model_path = None
        self.class_name_path = None
        self.anchors = None
        self.mask = None
        self.score_threshold = None
        self.postprocess_fn = None
        self.max_outputs = 100
        self.iou_threshold = 0.4
        self.names = None
        self.num_classes = None
        self.strides = None
