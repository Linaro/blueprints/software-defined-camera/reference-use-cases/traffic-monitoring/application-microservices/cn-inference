"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import os
import tvm
from tvm.contrib import graph_runtime
import numpy as np

from src.postprocess import postprocess_tiny, postprocess_yolo
from src.utils import  decode_class_names
from src.config import Config
from src.logger import get_logger
from src.custom_exception import ModelNotFoundError

logger = get_logger()

def load_model(model_path):
    """
    Load a TVM compiled model from a specified path.
    
    This function loads a custom yolov3 model from the given file path.
    
    Args:
        model_path (str): The path to the model file to be loaded.
        
    Returns:
        model: The loaded machine learning model.
        
    Raises:
        ModelNotFoundError: Raises custom exception, If the specified model file is not found at the given path.
    """
    if os.path.exists(model_path):
        model_json = os.path.join(model_path, "deploy_graph.json")
        model_lib = os.path.join(model_path, "deploy_lib.so")
        model_params = os.path.join(model_path, "deploy_param.params")

        m_j = os.path.exists(model_json)
        m_l = os.path.exists(model_lib)
        m_p = os.path.exists(model_params)

        if m_j and m_l and m_p:
            ctx = tvm.cpu()
            loaded_json = open(model_json).read()
            loaded_lib = tvm.runtime.load_module(model_lib)
            loaded_params = bytearray(open(model_params, "rb").read())
            model = graph_runtime.create(loaded_json, loaded_lib, ctx)
            model.load_params(loaded_params)
            logger.info("Model loaded successfully.")
            return model
        else:
            logger.error("""Model Not found, Make sure that deploy_graph.json,
                         deploy_lib & deploy_param.params is available inside %s folder.""", model_path)
            raise ModelNotFoundError("Model files not found.")
    else:
        raise ModelNotFoundError("Model Folder not found.")

def load_model_config(model, cpu_cores, conf_file="config/yolov3.json"):
    """
    Load and apply configuration settings for a model.

    This function loads configuration settings from a JSON file and applies them to the provided model.
    
    Args:
        model (Model): The model to which the configuration settings will be applied.
        cpu_cores (int): The number of CPU cores to be used for processing.
        conf_file (str, optional): The path to the JSON configuration file. Default is "config/yolov3.json".
        
    Returns:
        config: Return instance of config class with loaded configuration.
        
    Raises:
        FileNotFoundError: If the specified configuration file is not found.
        JSONDecodeError: If there is an error decoding the JSON content from the configuration file.
    """
    if os.path.exists(conf_file):
        config_json = json.load(open(conf_file))
        logger.info("Loaded model config for %s.", model)
    else:
        raise FileNotFoundError("No such a file or directory, unable to load the config file for model.")
    
    config = Config()

    os.environ["TVM_NUM_THREADS"] = cpu_cores

    config.model_path = config_json["model_path"]                                                                              
    config.class_name_path = config_json["class_name_path"]                                                                       
    config.strides = config_json["strides"]                                                                                                   
    config.anchors = config_json["anchors"]                                                        
    config.mask = config_json["mask"]                                                                                 
    config.score_threshold = float(config_json["score_threshold"])
    if model == "tiny_yolov3":
        config.postprocess_fn = postprocess_tiny
    elif model == "yolov3":
        config.postprocess_fn = postprocess_yolo


    config.max_outputs = 100                                                                               
    config.iou_threshold = 0.4 
    config.names = decode_class_names(config.class_name_path)                                                     
    config.num_classes = len(config.names)                                          
    config.anchors = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), config.anchors.split())))     
    config.mask = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), config.mask.split())))           
    config.strides = list(map(int, config.strides.split(',')))


    return config
