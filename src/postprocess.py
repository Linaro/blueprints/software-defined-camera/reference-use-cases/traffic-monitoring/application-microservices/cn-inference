"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import numpy as np

def _nms_boxes(boxes, scores, iou_thresh=0.45):
	"""
    Apply non-maximum suppression to filter out overlapping boxes.

    Args:
        boxes (numpy.ndarray): Bounding boxes to be filtered.
        scores (numpy.ndarray): Confidence scores corresponding to the boxes.
        iou_thresh (float, optional): Intersection over Union threshold for suppression.
                                      Default is 0.45.

    Returns:
        keep (numpy.ndarray): A list containing arrays of nms_boxes.
    """
	x = boxes[:, 0]
	y = boxes[:, 1]
	w = boxes[:, 2] -  boxes[:, 0]
	h = boxes[:, 3] -  boxes[:, 1]

	areas = (w)* (h)
	order = scores.argsort()[::-1]

	keep = []
	while order.size > 0:
		i = order[0]
		keep.append(i)

		xx1 = np.maximum(x[i], x[order[1:]])
		yy1 = np.maximum(y[i], y[order[1:]])
		xx2 = np.minimum(x[i] + w[i], x[order[1:]] + w[order[1:]])
		yy2 = np.minimum(y[i] + h[i], y[order[1:]] + h[order[1:]])

		w1 = np.maximum(0.0, xx2 - xx1 + 1)
		h1 = np.maximum(0.0, yy2 - yy1 + 1)
		inter = w1 * h1

		ovr = inter / (areas[i] + areas[order[1:]] - inter)
		inds = np.where(ovr <= iou_thresh)[0]
		order = order[inds + 1]

	keep = np.array(keep)
	return keep

def sigmoid(x):
	"""
    Compute the sigmoid function.

    Args:
        x (numpy.ndarray): Input array.

    Returns:
        numpy.ndarray: Output array after applying the sigmoid function element-wise.
    """
	return (1 / (1 + np.exp(-x)))

def postprocess_predictions(inputs, anchors, mask, strides, ind, num_classes=6):
	"""
    Post-processes model predictions for object detection using YoloV3.

    Args:
        inputs (numpy.ndarray): Model prediction inputs.
        anchors (numpy.ndarray): Anchor boxes used in the model.
        mask (numpy.ndarray): Mask values specifying anchor subsets.
        strides (list): List of stride values for the model.
        ind (int): Index of the desired anchors subset.
        num_classes (int, optional): Number of classes in the detection problem. Default is 2.

    Returns:
        tuple: A tuple containing arrays of all_boxes, all_scores, and all_classes.
               - all_boxes (numpy.ndarray): Bounding boxes of detected objects.
               - all_scores (numpy.ndarray): Confidence scores of detected objects.
               - all_classes (numpy.ndarray): Class labels of detected objects.
    """
	
	logits = inputs[0]
	anchors, stride = anchors[mask[ind]], strides[ind]
	x_shape = np.shape(logits)
	logits = np.reshape(logits, (x_shape[0], x_shape[1], x_shape[2], len(anchors), num_classes + 5))

	box_xy, box_wh, obj, cls = logits[...,:2], logits[...,2:4], logits[...,4], logits[...,5:]
	box_xy = sigmoid(box_xy)
	obj = sigmoid(obj)
	cls = sigmoid(cls)

	grid_shape = x_shape[1:3]
	grid_h, grid_w = grid_shape[0], grid_shape[1]
	anchors = np.array(anchors, dtype=np.float32)
	grid = np.meshgrid(np.arange(grid_w), np.arange(grid_h))
	grid = np.expand_dims(np.stack(grid, axis=-1), axis=2)  # [gx, gy, 1, 2]

	box_xy = (box_xy + np.array(grid, dtype=np.float32)) * stride
	box_wh = np.exp(box_wh) * np.array(anchors, dtype=np.float32)

	box_x1y1 = box_xy - box_wh / 2.
	box_x2y2 = box_xy + box_wh / 2.
	box = np.concatenate([box_x1y1, box_x2y2], axis=-1)

	all_boxes = np.reshape(box, (x_shape[0], -1, 1, 4))
	objects = np.reshape(obj, (x_shape[0], -1, 1))
	all_classes = np.reshape(cls, (x_shape[0], -1, num_classes))

	all_scores = objects * all_classes
	return all_boxes, all_scores, all_classes

def filter_boxes(all_boxes, all_scores, score_thresh=0.2):
	"""
    Filter boxes based on confidence scores & Non maximum suppression.

    Args:
        all_boxes (numpy.ndarray): Bounding boxes to be filtered.
        all_scores (numpy.ndarray): Confidence scores corresponding to the boxes.
        score_thresh (float, optional): Confidence score threshold for filtering.
                                        Default is 0.2.

    Returns:
        tuple: A tuple containing arrays of filtered_boxes, filtered_classes, and filtered_scores.
               - filtered_boxes (numpy.ndarray): Bounding boxes after filtering.
               - filtered_classes (numpy.ndarray): Class labels corresponding to the filtered boxes.
               - filtered_scores (numpy.ndarray): Confidence scores after filtering.
    """
	box_classes = np.argmax(all_scores, axis=-1)
	box_class_scores = np.max(all_scores, axis=-1)
	pos = np.where(box_class_scores >= score_thresh)

	fil_boxes = all_boxes[pos]
	fil_boxes = np.reshape(fil_boxes, (fil_boxes.shape[0], fil_boxes.shape[2]))
	fil_classes = box_classes[pos]
	fil_scores = box_class_scores[pos]

	nboxes, nclasses, nscores = [], [], []
	for c in set(fil_classes):
		inds = np.where(fil_classes == c)
		b = fil_boxes[inds]
		c = fil_classes[inds]
		s = fil_scores[inds]

		keep = _nms_boxes(b, s)

		nboxes.append(b[keep])
		nclasses.append(c[keep])
		nscores.append(s[keep])

	if not nclasses and not nscores:
		return None, None, None

	return np.concatenate(nboxes), np.concatenate(nclasses), np.concatenate(nscores)

def postprocess_yolo(model, anchors, mask, strides, max_outputs, iou_threshold, score_threshold, num_classes):
    """
    Post-processes model predictions for object detection using YOLO.

    Args:
        model: The machine learning model used for object detection.
        anchors (numpy.ndarray): Anchor boxes used in the model.
        mask (numpy.ndarray): Mask values specifying anchor subsets.
        strides (list): List of stride values for the model.
        max_outputs (int): Maximum number of output detections.
        iou_threshold (float): Intersection over Union threshold for non-maximum suppression.
        score_threshold (float): Confidence score threshold for detections.
        num_classes (int): Number of classes in the detection problem.
	
	Returns:
        Numpy ndarray: A Numpy arrays of right_boxes, right_classes, and right_scores.
               - right_boxes (numpy.ndarray): Bounding boxes of correct detections.
               - right_classes (numpy.ndarray): Class labels of correct detections.
               - right_scores (numpy.ndarray): Confidence scores of correct detections.
    """
    
    out0, out1, out2 = model.get_output(0).asnumpy(), model.get_output(1).asnumpy(), model.get_output(2).asnumpy()
    big_all_boxes, big_all_scores, big_all_classes = postprocess_predictions([out0], anchors, mask, strides, 0, num_classes)
    mid_all_boxes, mid_all_scores, mid_all_classes = postprocess_predictions([out1], anchors, mask, strides, 1, num_classes)
    small_all_boxes, small_all_scores, small_all_classes = postprocess_predictions([out2], anchors, mask, strides, 2, num_classes)

    bboxes = np.concatenate([big_all_boxes, mid_all_boxes, small_all_boxes], axis=1)
    scores = np.concatenate([big_all_scores, mid_all_scores, small_all_scores], axis=1)
    classes = np.concatenate([big_all_classes, mid_all_classes, small_all_classes], axis=1)
    right_boxes, right_classes, right_scores = filter_boxes(bboxes, scores, score_threshold)
    return right_boxes, right_classes, right_scores


def postprocess_tiny(model, anchors, mask, strides, max_outputs, iou_threshold, score_threshold, num_classes):
    """
    Post-processes model predictions for object detection using Tiny YOLOv3.

    Args:
        model: The machine learning model used for object detection.
        anchors (numpy.ndarray): Anchor boxes used in the model.
        mask (numpy.ndarray): Mask values specifying anchor subsets.
        strides (list): List of stride values for the model.
        max_outputs (int): Maximum number of output detections.
        iou_threshold (float): Intersection over Union threshold for non-maximum suppression.
        score_threshold (float): Confidence score threshold for detections.
        num_classes (int): Number of classes in the detection problem.

    Returns:
        Numpy ndarray: A Numpy arrays of right_boxes, right_classes, and right_scores.
               - right_boxes (numpy.ndarray): Bounding boxes of correct detections.
               - right_classes (numpy.ndarray): Class labels of correct detections.
               - right_scores (numpy.ndarray): Confidence scores of correct detections.
    """
    out0, out1 = model.get_output(0).asnumpy(), model.get_output(1).asnumpy()
    big_all_boxes, big_all_scores, big_all_classes = postprocess_predictions([out0], anchors, mask, strides, 0, num_classes)
    small_all_boxes, small_all_scores, small_all_classes = postprocess_predictions([out1], anchors, mask, strides, 1, num_classes)

    bboxes = np.concatenate([big_all_boxes, small_all_boxes], axis=1)
    scores = np.concatenate([big_all_scores, small_all_scores], axis=1)
    classes = np.concatenate([big_all_classes, small_all_classes], axis=1)
    right_boxes, right_classes, right_scores = filter_boxes(bboxes, scores, score_threshold)
    return right_boxes, right_classes, right_scores

